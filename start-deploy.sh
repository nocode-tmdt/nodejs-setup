. ./env.sh

docker-compose -f docker-compose.yml -p $APP_NAME_DOCKER down

docker-compose -f docker-compose.yml -p $APP_NAME_DOCKER up --build -d
